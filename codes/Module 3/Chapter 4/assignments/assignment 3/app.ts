//canvas declaration
var canvas: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context: CanvasRenderingContext2D = canvas.getContext("2d");
context.save();
context.translate(0,500);
context.scale(1,-1);


function and(){            //and gate

    context.clearRect(0,0,canvas.width,canvas.height);
                //error handeling
    var x = +prompt("Enter the X coordinate between 0 to 800");
    if( (x > 800) || (x < 0) ) 
    {
        var x = +prompt("enter a valid choice X-cordinate should be between 0 to 800 ");  
    }
    var y = +prompt("Enter the Y coordinate between 0 to 500");
    if( (y > 500) || (x < 0))
    {
        var x = +prompt("enter a valid choice Y-cordinate should be between 0 to 500 ");  
    }
    var y1 = new Geometry.line(context,x,y-15,x-40,y-15,"blue");
    y1.draw(); 
    var y2 = new Geometry.line(context,x,y+15,x-40,y+15,"blue");
    y2.draw(); 
    var l1 = new Geometry.line(context,x,y-30,x,y+30,"black");
    l1.draw();  
    var l2 = new Geometry.line(context,x,y-30,x+20,y-30,"black");
    l2.draw();
    var l3 = new Geometry.line(context,x,y+30,x+20,y+30,"black");
    l3.draw();
    // arc for AND gate .....
    var arc1 = new Geometry.arc(context,x+20,y,30,"black");
    arc1.draw();
    var l4 = new Geometry.line(context,x+50,y,x+80,y,"black");
    l4.draw();
    
}    // function for not gate
function not(){

    context.clearRect(0,0,canvas.width,canvas.height);

    var x = +prompt("Enter the X coordinate between 0 to 800");
    if( (x > 800) || (x < 0) ){
        var x = +prompt("enter a valid choice X-cordinate should be between 0 to 800");  
    }
    var y = +prompt("Enter the Y coordinate between 0 to 500");
    if( (y > 550) || (x < 0) ){
        var x = +prompt("enter a valid choice Y-cordinate should be between 0 to 500 ");  
    }
    var x1 = new Geometry.line(context,x,y-15,x-40,y-15,"blue");
    x1.draw(); 
    var x2 = new Geometry.line(context,x,y+15,x-40,y+15,"blue");
    x1.draw(); 
    var l1 = new Geometry.line(context,x,y-30,x,y+30,"blue");
    l1.draw();  
    var l2 = new Geometry.line(context,x,y-30,x+20,y-30,"blue");
    l2.draw();
    var l3 = new Geometry.line(context,x,y+30,x+20,y+30,"blue");
    l3.draw();
    var arc1 = new Geometry.arc(context,x+20,y,30,"blue");
    arc1.draw();
    // circle for not gate
    var cir1 = new Geometry.circle(context,x+60,y,10,"blue");
    cir1.draw();
    var l4 = new Geometry.line(context,x+70,y,x+100,y+0,"blue");
    l4.draw();
}