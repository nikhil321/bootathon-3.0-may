namespace Geometry{

    export class line { //line class
        public pt1x:number;
        public pt1y:number;
        public pt2x:number;
        public pt2y:number;
        public context:CanvasRenderingContext2D;
        private ang:number;
        public color:string;
        constructor(context:CanvasRenderingContext2D,pt1x:number,pt1y:number,pt2x:number,pt2y:number,color:string){
            this.pt1x = pt1x;
            this.pt2x = pt2x;
            this.pt1y = pt1y;
            this.pt2y = pt2y;
            this.context = context;
            this.color=color;
        }

        draw(){ //drawing line with function
        this.context.beginPath();
        this.context.moveTo(this.pt1x, this.pt1y);
        this.context.lineTo(this.pt2x, this.pt2y);
        this.context.lineWidth = 4;
        this.context.strokeStyle = this.color;
        this.context.stroke();

        }
    }
                                           // class circle
                                           //for drawing of NOT gate
    export class circle{
        public c1:number;
        public c2:number;
        public r:number;
        public context:CanvasRenderingContext2D;
        public color:string;
        constructor(context:CanvasRenderingContext2D,c1:number,c2:number,radius:number,color:string){
            this.context = context;
            this.c1 = c1;
            this.c2 = c2;
            this.r = radius;
            this.color = color;
        }
        draw(){
            this.context.beginPath();
            this.context.arc(this.c1, this.c2, this.r, 0 ,2* Math.PI );
            this.context.lineWidth = 4;
            this.context.strokeStyle = "black";
            this.context.fillStyle = this.color;
            this.context.fill();
            this.context.stroke();
        }
    }

    export class arc{                // class arc
        
        public c1:number;
        public c2:number;
        public r:number;
        public context:CanvasRenderingContext2D;
        public color:string;
        constructor(context:CanvasRenderingContext2D,c1:number,c2:number,radius:number,color:string){
            this.context = context;
            this.c1 = c1;
            this.c2 = c2;
            this.r = radius;
            this.color = color;
        }
        draw(){
            this.context.beginPath();
            this.context.arc(this.c1, this.c2, this.r, -Math.PI/2 , Math.PI / 2);
            this.context.lineWidth = 4;
            this.context.strokeStyle = "blue";
            this.context.fillStyle = this.color;
            this.context.stroke();
        }
    }

}