var Geometry;
(function (Geometry) {
    class line {
        constructor(context, pt1x, pt1y, pt2x, pt2y, color) {
            this.pt1x = pt1x;
            this.pt2x = pt2x;
            this.pt1y = pt1y;
            this.pt2y = pt2y;
            this.context = context;
            this.color = color;
        }
        draw() {
            this.context.beginPath();
            this.context.moveTo(this.pt1x, this.pt1y);
            this.context.lineTo(this.pt2x, this.pt2y);
            this.context.lineWidth = 4;
            this.context.strokeStyle = this.color;
            this.context.stroke();
        }
    }
    Geometry.line = line;
    // class circle
    //for drawing of NOT gate
    class circle {
        constructor(context, c1, c2, radius, color) {
            this.context = context;
            this.c1 = c1;
            this.c2 = c2;
            this.r = radius;
            this.color = color;
        }
        draw() {
            this.context.beginPath();
            this.context.arc(this.c1, this.c2, this.r, 0, 2 * Math.PI);
            this.context.lineWidth = 4;
            this.context.strokeStyle = "black";
            this.context.fillStyle = this.color;
            this.context.fill();
            this.context.stroke();
        }
    }
    Geometry.circle = circle;
    class arc {
        constructor(context, c1, c2, radius, color) {
            this.context = context;
            this.c1 = c1;
            this.c2 = c2;
            this.r = radius;
            this.color = color;
        }
        draw() {
            this.context.beginPath();
            this.context.arc(this.c1, this.c2, this.r, -Math.PI / 2, Math.PI / 2);
            this.context.lineWidth = 4;
            this.context.strokeStyle = "blue";
            this.context.fillStyle = this.color;
            this.context.stroke();
        }
    }
    Geometry.arc = arc;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map