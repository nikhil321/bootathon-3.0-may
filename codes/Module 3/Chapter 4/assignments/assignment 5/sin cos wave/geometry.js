var Geometry;
(function (Geometry) {
    class Point // class declaring points..
     {
        constructor(x, y) {
            this._x = x;
            this._y = y;
        }
        get x() {
            return this._x;
        }
        get y() {
            return this._y;
        }
        set x(_x) {
            this._x = _x;
        }
        set y(_y) {
            this._y = _y;
        }
    }
    Geometry.Point = Point;
    class sinewave //class for sine wave
     {
        constructor(pt, amp) {
            this.data = [];
            this.count = 0;
            this.xscale = 1.5;
            this.srtPt = pt;
            this.amp = amp;
            this.motion = "front";
            this.calculate();
        }
        calculate() {
            for (let i = 0; i <= 360; i++) {
                var x = this.xscale * i;
                var y = this.amp * Math.sin(i * (Math.PI / 180));
                this.data.push(new Point(x, y));
            }
        }
        draw(context) {
            context.save();
            context.translate(this.srtPt.x, this.srtPt.y);
            context.scale(1, -1);
            context.beginPath();
            context.moveTo(this.data[0].x, this.data[0].y);
            for (let i = 1; i <= 360; i++) {
                context.lineTo(this.data[i].x, this.data[i].y);
            }
            context.strokeStyle = "red";
            context.stroke();
            if (this.motion == "front") {
                this.count++;
            }
            else if (this.motion == "back") {
                this.count--;
            }
            context.restore();
            context.lineWidth = 5;
        }
    }
    Geometry.sinewave = sinewave;
    //class for cosine wave...
    class coswave {
        constructor(pt, amp) {
            this.data = [];
            this.count = 0;
            this.xscale = 1.5;
            this.srtPt = pt;
            this.amp = amp;
            this.motion = "front";
            this.calculate();
        }
        calculate() {
            for (let i = 0; i <= 360; i++) {
                var x = this.xscale * i;
                var y = this.amp * Math.cos(i * (Math.PI / 180));
                this.data.push(new Point(x, y));
            }
        }
        draw(context) {
            context.save();
            context.translate(this.srtPt.x, this.srtPt.y);
            context.scale(1, -1);
            context.beginPath();
            context.moveTo(this.data[0].x, this.data[0].y);
            for (let i = 1; i <= 360; i++) {
                context.lineTo(this.data[i].x, this.data[i].y);
            }
            context.strokeStyle = "blue";
            context.stroke();
            if (this.motion == "front") {
                this.count++;
            }
            else if (this.motion == "back") {
                this.count--;
            }
            context.restore();
            context.lineWidth = 5;
        }
    }
    Geometry.coswave = coswave;
})(Geometry || (Geometry = {}));
//# sourceMappingURL=geometry.js.map