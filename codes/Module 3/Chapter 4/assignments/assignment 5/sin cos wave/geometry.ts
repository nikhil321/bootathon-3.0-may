namespace Geometry 
{                                       
    export class Point              // class declaring points..
    {                               
        private _x : number;
        private _y : number;
        constructor(x:number,y:number) 
        {
            this._x = x;
            this._y = y;
        }
        get x() : number {
          return this._x;
        }
        get y() : number {
            return this._y;
        }
        set x(_x : number ){
            this._x = _x;
        }
        set y(_y : number) {
            this._y = _y;
        }
    }
   export class sinewave                      //class for sine wave
   {
       private data  : Point[] = [];
       private count : number = 0;
      
       private amp : number;
       private xscale : number = 1.5;
       private srtPt : Point;
       private motion : string;
       
       constructor(pt : Point,amp : number) 
       {
           this.srtPt = pt;
           this.amp = amp;
           this.motion = "front";
           this.calculate();
       }

                                                   
       private calculate() //to measure the angle....
       {
           for(let i=0;i<=360;i++) 
           {
               var x = this.xscale * i;
               var y = this.amp * Math.sin(i*(Math.PI/180));
               this.data.push(new Point(x,y));
               
           }
       }


       draw(context : CanvasRenderingContext2D) 
       {
            context.save();
            context.translate(this.srtPt.x,this.srtPt.y);
            context.scale(1,-1);
            context.beginPath();
            context.moveTo(this.data[0].x,this.data[0].y);
            for(let i=1;i<=360;i++) {
                context.lineTo(this.data[i].x,this.data[i].y);
            }
            context.strokeStyle = "red";
            context.stroke();
            
            if(this.motion == "front") {
                this.count++;
            }
            else if(this.motion == "back") {
                this.count--;
            }
            context.restore();
            context.lineWidth = 5;
       }

   }
                                              //class for cosine wave...
   export class coswave 
   {
    private data  : Point[] = [];
    private count : number = 0;
    private amp : number;
    private xscale : number = 1.5;
    private srtPt : Point;
    private motion : string;
    
    constructor(pt : Point,amp : number) 
    {
        this.srtPt = pt;
        this.amp = amp;
        this.motion = "front";
        this.calculate();
    }

    

    private calculate() {
        for(let i=0;i<=360;i++) {
            var x = this.xscale * i;
            var y = this.amp * Math.cos(i*(Math.PI/180));
            this.data.push(new Point(x,y));
        }
    }

    draw(context : CanvasRenderingContext2D) 
    {
         context.save();
         context.translate(this.srtPt.x,this.srtPt.y);
         context.scale(1,-1);
         context.beginPath();
         context.moveTo(this.data[0].x,this.data[0].y);
         for(let i=1;i<=360;i++) {
             context.lineTo(this.data[i].x,this.data[i].y);
         }
         context.strokeStyle = "blue";

         context.stroke();

         if(this.motion == "front") {
             this.count++;
         }
         else if(this.motion == "back") {
             this.count--;
         }
       
         context.restore();
         context.lineWidth = 5;
    }

    
    
    
}
}