  //canvas creation
var canvas : HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context : CanvasRenderingContext2D = canvas.getContext("2d");
// for waves
var wave1 : Geometry.sinewave = new Geometry.sinewave(new Geometry.Point(200,350),300);
var wave2 : Geometry.coswave = new Geometry.coswave(new Geometry.Point(200,350),300);

animation();
   // function draws the curve
function animation() {
    context.clearRect(0,0,canvas.width,canvas.height);
    wave1.draw(context);
    wave2.draw(context);
    window.requestAnimationFrame(animation);
}
