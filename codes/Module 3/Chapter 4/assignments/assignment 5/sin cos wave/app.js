//canvas creation
var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
// for waves
var wave1 = new Geometry.sinewave(new Geometry.Point(200, 350), 300);
var wave2 = new Geometry.coswave(new Geometry.Point(200, 350), 300);
animation();
// function draws the curve
function animation() {
    context.clearRect(0, 0, canvas.width, canvas.height);
    wave1.draw(context);
    wave2.draw(context);
    window.requestAnimationFrame(animation);
}
//# sourceMappingURL=app.js.map