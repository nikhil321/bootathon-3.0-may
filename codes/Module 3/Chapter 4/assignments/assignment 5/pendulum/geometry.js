var geometry;
(function (geometry) {
    class Point //point class
     {
        constructor(x, y) {
            this.x = x;
            this.y = y;
        }
    }
    geometry.Point = Point;
    class circle {
        constructor(p1, p2, context, color) {
            this.p1 = p1;
            this.p2 = p2;
            this.context = context;
            this.color = color;
            this.i = 90;
            this.r = 50,
                this.left = true;
            this.right = false;
        }
        draw() {
            this.context.beginPath();
            this.context.moveTo(this.p1.x, this.p1.y);
            this.context.lineTo(this.p2.x, this.p2.y);
            this.context.lineWidth = 4;
            this.context.strokeStyle = this.color;
            this.context.stroke();
            this.context.beginPath();
            this.context.moveTo(this.p1.x - 100, this.p1.y);
            this.context.lineTo(this.p1.x + 100, this.p1.y);
            this.context.lineWidth = 4;
            this.context.strokeStyle = "black";
            this.context.stroke();
            this.context.beginPath();
            this.context.arc(this.p2.x, this.p2.y, this.r, 0, 2 * Math.PI);
            this.context.lineWidth = 4;
            this.context.fillStyle = this.color;
            this.context.fill();
            this.context.strokeStyle = "black";
            this.context.stroke();
        }
        update() {
            //distance formula....
            var len = Math.sqrt(Math.pow(this.p1.x - this.p2.x, 2) + Math.pow(this.p1.y - this.p2.y, 2));
            if (this.i < 150 && this.left) { //from mid to left
                this.i++;
                this.p2.x = this.p1.x + len * Math.cos(this.i * Math.PI / 180); //angle update
                this.p2.y = this.p1.y + len * Math.sin(+this.i * Math.PI / 180);
            }
            if (this.i == 150) { // at 150 its return to right
                this.left = false;
                this.right = true;
            }
            if (this.i >= 50 && this.right) { //from left to direct right        
                this.i--;
                this.p2.x = this.p1.x + len * Math.cos(this.i * Math.PI / 180); // angle update
                this.p2.y = this.p1.y + len * Math.sin(this.i * Math.PI / 180);
            }
            if (this.i == 60) { // at 60 its return to right side
                this.left = true;
                this.right = false;
            }
        }
    }
    geometry.circle = circle;
})(geometry || (geometry = {}));
//# sourceMappingURL=geometry.js.map