// canvas creation
var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
context.translate(400, 300);
// dial creation
var circle = new Circle(context, 0, 0, 150, "blue");
circle.draw();
// line creation
var lin = new Line(context, 0, 0, 150, 0, "black");
lin.draw();
// function to rotate the needle
function rotate() {
    context.clearRect(300, 300, canvas.width, canvas.height);
    var val = document.getElementById("t1");
    var i = document.getElementById("t2");
    var val1 = parseFloat(val.value);
    var x = 130 * Math.cos(-val1 * Math.PI / 180);
    var y = 130 * Math.sin(-val1 * Math.PI / 180);
    var circle = new Circle(context, 0, 0, 150, "red");
    circle.draw();
    var l1 = new Line(context, 0, 0, x, y, "black");
    l1.draw();
}
//# sourceMappingURL=app.js.map