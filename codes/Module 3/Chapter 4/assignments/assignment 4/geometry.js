class Line ///line class 
 {
    constructor(context, ref1X, ref1Y, ref2X, ref2Y, color) {
        this.ref1X = ref1X;
        this.ref2X = ref2X;
        this.ref1Y = ref1Y;
        this.ref2Y = ref2Y;
        this.context = context;
        this.color = color;
    }
    draw() {
        this.context.beginPath();
        this.context.moveTo(this.ref1X, this.ref1Y);
        this.context.lineTo(this.ref2X, this.ref2Y);
        this.context.lineWidth = 4;
        this.context.strokeStyle = this.color;
        this.context.stroke();
    }
}
// class circle
class Circle {
    constructor(context, x, y, r, color) {
        this.refX = x;
        this.refY = y;
        this.context = context;
        this.ang = 0;
        this.r = r;
        this.color = color;
    }
    draw() {
        this.context.beginPath();
        this.context.arc(this.refX, this.refY, this.r, 0, Math.PI * 2);
        this.context.lineWidth = 4;
        this.context.fillStyle = this.color;
        this.context.fill();
        this.context.strokeStyle = this.color;
        this.context.stroke();
        this.context.beginPath();
        this.context.font = "20pt Brush Script MT";
        this.context.fillText("90", this.refX, this.refY - 180);
        this.context.fillText("180", this.refX - 196, this.refY);
        this.context.fillText("270", this.refX, this.refY + 180);
        this.context.fillText("0", this.refX + 180, this.refY);
        this.context.fillStyle = "black";
        this.context.fill();
    }
}
//# sourceMappingURL=geometry.js.map