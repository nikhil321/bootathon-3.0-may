// for graphs 1

declare var drawgraph2;         //Framework function
declare var drawgraph;          //Framework function

var datapoints1:{x:number,y:number}[] = [];
var datapoints2:{x:number,y:number}[] = [];

for(var i=1;i<=10;i++)
{
    datapoints1.push({ x: i, y: i * i });
    datapoints2.push({ x: i, y: i * i + i });
}
drawgraph("l1", datapoints1, "x axis", "x squared");

function draw2()
{
    drawgraph2("l2", datapoints1, datapoints2, "X-axis", "Y-axis","comparision","x^2","x^2+x");
}

//for graph3

declare var graphline;         //Framework function         

var datapoints1:{x:number,y:number}[] = [];
var datapoints2:{x:number,y:number}[] = [];

datapoints1.push({ x: 1, y: 1 });
datapoints1.push({ x: 2, y: 1 });
datapoints1.push({ x: 2, y: 2 });
datapoints1.push({ x: 1, y: 2 });
datapoints1.push({ x: 1, y: 1 });
function draw3()
{
graphline("l3", datapoints1, "x axis", "y-axis");
}


//for g4

declare var graphlogx;         //Framework function         

var data1:{x:number,y:number}[] = [];

data1.push({ x: 10, y: 10 });
        
data1.push({ y: 15, x: 640 });
data1.push({ y: 22, x: 10240 });
data1.push({ y: 31, x: 80240 });
data1.push({ y: 42, x: 162400 });
data1.push({ y: 55, x: 200000 });
data1.push({ y: 70, x: 220000 });
data1.push({ y: 87, x: 230000 });
data1.push({ y: 100, x: 256000 });
function draw4()
{
graphlogx("l4",data1,"x-Axis","y-Axis");
}


//for g5

declare var graphlogy;         //Framework function         

var data1:{x:number,y:number}[] = [];

data1.push({ x: 10, y: 10 });

data1.push({ x: 15, y: 640 });
data1.push({ x: 22, y: 10240 });
data1.push({ x: 31, y: 80240 });
data1.push({ x: 42, y: 162400 });
data1.push({ x: 55, y: 200000 });
data1.push({ x: 70, y: 220000 });
data1.push({ x: 87, y: 230000 });
data1.push({ x: 100, y: 256000 });
function draw5()
{
graphlogy("l5",data1,"X-axis","Y-axis");
}

//for g6

declare var logymultiple;
var data1:{x:number,y:number}[] = [];
var data2:{x:number,y:number}[] = [];

data1.push({ x: 10, y: 10 });
data1.push({ x: 15, y: 640 });
data1.push({ x: 22, y: 10240 });
data1.push({ x: 31, y: 80240 });
data1.push({ x: 42, y: 162400 });
data1.push({ x: 55, y: 200000 });
data1.push({ x: 70, y: 220000 });
data1.push({ x: 87, y: 230000 });
data1.push({ x: 100, y: 256000 });


data2.push({ x: 10, y: 10 });
data2.push({ x: 15, y: 840 });
data2.push({ x: 22, y: 15240 });
data2.push({ x: 31, y: 99240 });
data2.push({ x: 42, y: 202400 });
data2.push({ x: 55, y: 300000 });
data2.push({ x: 70, y: 330000 });
data2.push({ x: 87, y: 400000 });
data2.push({ x: 100, y: 456000 });

var data=[];
data.push({type:"line",dataPoints:data1});
data.push({type:"line",dataPoints:data2});
function draw6()
{
    logymultiple("l6","X axis","Y-axis",data,"log");
}


//for g7

declare var graphline;

var datapoints1:{x:number,y:number}[] = [];
var datapoints2:{x:number,y:number}[] = [];
datapoints1.push({ x: 10, y: 10 });
datapoints1.push({ x: 20, y: 10 });
datapoints1.push({ x: 20, y: 20 });
datapoints1.push({ x: 10, y: 20 });
datapoints1.push({ x: 10, y: 10 });

datapoints2.push({ x: 10, y: 10 });
datapoints2.push({ x: 20, y: 20 });
datapoints2.push({ x: 30, y: 30 });
datapoints2.push({ x: 40, y: 40 });
datapoints2.push({ x: 50, y: 50 });

var data = [];
data.push({
    type: "spline",
    xValueType: "Float",
    showInLegend: false,
    name: "f(x)",
    markerSize: 1,
    dataPoints: datapoints1
})
                        
data.push({
    type: "line",
    xValueType: "Float",
    showInLegend: false,
    name: "f(x1)",
    markerSize: 1,
    dataPoints: datapoints2
})                 
function draw7()
{
graphline("l7", data, "", "",100,-100);
}

