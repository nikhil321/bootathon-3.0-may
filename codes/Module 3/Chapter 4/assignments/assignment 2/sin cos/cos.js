var datapoints1 = [];
for (var i = 0; i <= 4 * Math.PI; i = i + 0.01) {
    datapoints1.push({ x: i, y: Math.cos(i) });
}
function draw() {
    graphline("l1", datapoints1, "X-AXIS ", "Y-AXIS");
}
//# sourceMappingURL=cos.js.map