// select no of col and rows 
var s1: HTMLSelectElement = <HTMLSelectElement>document.getElementById("s1");
var t1: HTMLTableElement  = <HTMLTableElement>document.getElementById("t1");
for(let i=1;i<=10 ;i++)
{
    var option: HTMLOptionElement =<HTMLOptionElement>document.createElement("option");
    option.text = i.toString();
    option.value = i.toString();
    s1.add(option);    
}

function create()
{
    deletetable(t1);
    createtable(t1,"a");
}
// clear the last table...
function deletetable(t1){
    while(t1.rows.length>0)
    {
        t1.deleteRow(0);
    }
}
// creation of tables...
function createtable(t1:HTMLTableElement,id:string){
    var row =+s1.value;
    var col =2;
    for(let i=0;i<row;i++)
    {
        var trow:HTMLTableRowElement =t1.insertRow();
        for(let j=0;j< col;j++)
        {
            var t1cell =trow.insertCell();
            var text:HTMLInputElement =<HTMLInputElement>document.createElement("input");
            text.id =id + i + j;
            text.type="number";
            t1cell.appendChild(text);
        }
    }
    
}
function resultant()
{
    var a:number[][]=[];
    readmat(a,+s1.value,2,"a");
    calculate(a);
}

// read the tables....
function readmat(a: number[][],row:number,col:number,id:string){
    for(let i=0;i<row;i++)
    {
        a[i] = [];
        for(let j=0;j<col;j++)
        {
            let t1: HTMLInputElement=<HTMLInputElement>document.getElementById(id + i + j);
            a[i][j] = +t1.value;

        }
    }

}

// calculation of resultant of forces 
function calculate(a:number[][])
{
    var fx:number=0;
    var fy:number=0;
    var theta:number;
    for(let i=0;i<+s1.value;i++)
    {
        fx +=a[i][0]*Math.cos(Math.PI/180 * a[i][1]);   // summation of x
        fy +=a[i][0]*Math.sin(Math.PI/180 * a[i][1]);   // summation of y
    }
    var result =Math.sqrt(Math.pow(fx,2)+Math.pow(fy,2)); //resultant magnitude 
    theta = Math.atan2(fy,fx);    //resultant angle
    //console.log("mag",result);
    //console.log("angle", theta* 180 / Math.PI);

document.getElementById("p").innerHTML="Resultant of the forces is : "+result+  "units" +"<br/>"+"Angle of the resultant force is : "+theta*180/Math.PI+ " degree";
}